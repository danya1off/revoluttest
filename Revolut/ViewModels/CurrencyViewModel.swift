//
//  CurrencyViewModel.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/23/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class CurrencyViewModel {

    typealias Response = (ErrorService?) -> Void

    private var modelFacade: ModelFacadeServiceProtocol
    private var currencies = [Currency]()
    private var selectedCurrencies = [Currency]()
    private var fetchedSelectedCurrencies = [[Currency]]()

    init(_ modelFacade: ModelFacadeServiceProtocol = ModelFacadeService()) {
        self.modelFacade = modelFacade
    }

    var currenciesCount: Int {
        return currencies.count
    }

    var selectedCurrenciesCount: Int {
        return selectedCurrencies.count
    }

    private var getFirstSelectedCurrency: Currency {
        return selectedCurrencies[0]
    }

    var getFirstSelectedCurrencyCode: String {
        return getFirstSelectedCurrency.currencyCode
    }

    var notSelectableCurrencies: [String] {
        var disabledCurrencies = [Currency]()
        for currency in fetchedSelectedCurrencies {
            if currency[0] == getFirstSelectedCurrency {
                disabledCurrencies.append(currency[1])
            }
        }
        return disabledCurrencies.map { $0.currencyCode }
    }

    private func getCurrency(_ index: Int) -> Currency? {
        return index > currenciesCount ? nil : currencies[index]
    }

    func getCurrencyCellViewModel(by index: Int) -> CurrencyCellViewModel? {
        guard let currency = getCurrency(index) else { return nil }
        return CurrencyCellViewModel(currencyCode: currency.currencyCode,
                                     currencyName: currency.currencyName)
    }

    func addCurrency(_ index: Int) -> Bool {
        guard let currency = getCurrency(index) else { return false }
        if !selectedCurrencies.isEmpty,
            getFirstSelectedCurrencyCode == currency.currencyCode,
            selectedCurrencies.count == 2 {
            return false
        }
        selectedCurrencies.append(currency)
        return true
    }

    func getCurrencies(completion: @escaping Response) {
        modelFacade.readCurrencies { [weak self] result in
            switch result {
            case .success(let currencies):
                self?.currencies = currencies
                completion(.none)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func fetchAndSaveRates(completion: @escaping Response) {
        let exchangeRate = ExchangeRate(leftCurrency: selectedCurrencies[0], rightCurrency: selectedCurrencies[1])
        // fetch exchange rates from rest api and save them in core data
        modelFacade.getRates(for: [exchangeRate]) { error in
            completion(error)
        }
        self.selectedCurrencies = []
    }

    func fetchSelectedCurrencies(completion: @escaping (ErrorService?) -> Void) {
        modelFacade.fetchSelectedCurrencies { [weak self] result in
            switch result {
            case .success(let currencies):
                self?.fetchedSelectedCurrencies = currencies
                completion(.none)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func cancel() {
        self.selectedCurrencies = []
    }
}
