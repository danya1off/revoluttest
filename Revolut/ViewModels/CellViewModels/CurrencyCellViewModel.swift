//
//  CurrencyCellViewModel.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct CurrencyCellViewModel {
    var currencyCode: String
    var currencyName: String
}
