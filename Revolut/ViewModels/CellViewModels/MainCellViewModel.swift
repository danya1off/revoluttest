//
//  MainCellViewModel.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct MainCellViewModel {
    var leftCurrencyCode: String
    var leftCurrencyName: String
    var rightCurrencyName: String
    var exchangeAmount: Double

    init(_ exchangeRate: ExchangeRate) {
        leftCurrencyCode = exchangeRate.leftCurrency.currencyCode
        leftCurrencyName = exchangeRate.leftCurrency.currencyName
        rightCurrencyName = exchangeRate.rightCurrency.currencyName
        exchangeAmount = exchangeRate.rightCurrency.exchangeAmount
    }
}
