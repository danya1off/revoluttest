//
//  ViewModel.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

protocol BaseViewModel {

}

class MainViewModel: BaseViewModel {
    private var modelFacade: ModelFacadeServiceProtocol
    private var context: NSManagedObjectContext
    private var networkService: NetworkService
    private var currencies = [Currency]()
    private var selectedCurrencies = [Currency]()
    private var timer: Timer?

    init(_ modelFacade: ModelFacadeServiceProtocol = ModelFacadeService(),
         context: NSManagedObjectContext = CoreDataContext.shared.viewContext) {
        self.context = context
        self.modelFacade = modelFacade
        self.networkService = NetworkService()
    }

    private lazy var fetchedResultController: NSFetchedResultsController<RExchangeRate> = {
        let fetchRequest: NSFetchRequest<RExchangeRate> = RExchangeRate.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }()

    func fetchData(_ delegate: NSFetchedResultsControllerDelegate) throws {
        fetchedResultController.delegate = delegate
        do {
            try fetchedResultController.performFetch()
        } catch {
            throw ErrorService.noData
        }
    }

    func setupTimer(completion: @escaping (ErrorService?) -> Void) {
        if numberOfRows() > 0 {
            timer = timer ?? Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
                self?.updateRates(completion: completion)
            }
        } else {
            invalidateTimer()
        }
    }

    func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }

    func checkConnection(completion: @escaping (ErrorService?) -> Void) {
        networkService.checkConnection { [weak self] status in
            switch status {
            case .online:
                self?.setupTimer(completion: completion)
            case .offline, .nan:
                if self?.timer != nil  {
                    self?.invalidateTimer()
                    completion(.connectionError)
                }
            }
        }
    }

    private func updateRates(completion: @escaping (ErrorService?) -> Void) {
        guard let objects = fetchedResultController.fetchedObjects, !objects.isEmpty else {
            completion(.none)
            return
        }
        let convertedRates = objects.compactMap { $0.exchangeRate }
        modelFacade.getRates(for: convertedRates, completion: completion)
    }

    func numberOfRows() -> Int {
        if let objects = fetchedResultController.fetchedObjects {
            return objects.count
        }
        return 0
    }

    func getRate(at indexPath: IndexPath) -> MainCellViewModel? {
        let entity = fetchedResultController.object(at: indexPath)
        if let exchangeRate = entity.exchangeRate {
            return MainCellViewModel(exchangeRate)
        }
        return nil
    }

    func deleteRate(at indexPath: IndexPath) {
        let rate = fetchedResultController.object(at: indexPath)
        context.delete(rate)
        try? context.save()
    }

}
