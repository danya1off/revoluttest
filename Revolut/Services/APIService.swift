//
//  APIService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

protocol APIServiceProtocol {
    typealias ResponseResult<T: Codable> = (Result<T, ErrorService>) -> Void

    func getRates(_ rates: String, completion: @escaping ResponseResult<Dictionary<String, Double>>)
}

class APIService: APIServiceProtocol {

    private var urlSession: URLSession

    // MARK: - Initializer
    init(with urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }

    func getRates(_ rates: String, completion: @escaping ResponseResult<Dictionary<String, Double>>) {
        return request(router: Router.getRates(rates), completion: completion)
    }

}

extension APIService {

    private func request<T: Codable>(router: Router, completion: @escaping (Result<T, ErrorService>) -> Void) {
        do {
            let task = try urlSession.dataTask(with: router.request()) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        completion(.failure(.connectionError))
                        return
                    }
                    guard let httpResponse = response as? HTTPURLResponse else {
                        completion(.failure(.emptyResponseError))
                        return
                    }
                    guard (200...299).contains(httpResponse.statusCode) else {
                        let error: ErrorService
                        switch httpResponse.statusCode {
                        case 404:
                            error = .pageNotFound
                        case 500:
                            error = .serverError
                        default: error = .defaultError("Wrong response from server")
                        }
                        completion(.failure(error))
                        return
                    }

                    guard let data = data else {
                        completion(.failure(.noData))
                        return
                    }

                    do {
                        let decodedData = try JSONDecoder().decode(T.self, from: data)
                        completion(.success(decodedData))
                    } catch {
                        completion(.failure(.parseError))
                    }
                }

            }
            task.resume()
        } catch let error {
            completion(.failure(.defaultError(error.localizedDescription)))
        }

    }

}
