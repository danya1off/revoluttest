//
//  Router.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

enum Router {
    private static let baseUrl = "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"

    case getRates(String)

    private var path: String {
        switch self {
        case .getRates(let rates):
            return "?\(rates)"

        }
    }

    private enum HTTPMethod {
        case get
        case post
        case put
        case delete

        var value: String {
            switch self {
            case .get:
                return "GET"
            case .post:
                return "POST"
            case .put:
                return "PUT"
            case .delete:
                return "DELETE"
            }
        }
    }

    private var method: HTTPMethod {
        switch self {
        case .getRates: return .get
        }
    }

    func request() throws -> URLRequest {

        let urlString = Router.baseUrl + path

        guard let url = URL(string: urlString) else { throw ErrorService.incorrectUrl }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 5)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method.value


        return request
    }

}
