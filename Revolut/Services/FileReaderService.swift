//
//  FileReaderService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

enum FileFormat: String {
    case json = "json"
    case txt = "txt"
    case pdf = "pdf"
}

protocol FileReaderProtocol {
    typealias ResponseResult<T: Codable> = (Result<T, ErrorService>) -> Void
    func readCurrencies(from file: String, fileFormat format: FileFormat, completion: @escaping ResponseResult<[Currency]>)
}

class FileReaderService: FileReaderProtocol {

    private var bundle: Bundle

    init(_ bundle: Bundle = .main) {
        self.bundle = bundle
    }

    func readCurrencies(from file: String, fileFormat format: FileFormat, completion: @escaping ResponseResult<[Currency]>) {
        return self.readFile(fileName: file, fileFormat: format, completion: completion)
    }

    private func readFile<T: Codable>(fileName name: String,
                                      fileFormat format: FileFormat,
                                      completion: @escaping (Result<T, ErrorService>) -> Void) {
        guard let path = bundle.path(forResource: name, ofType: format.rawValue) else {
            completion(.failure(.fileNotFound("\(name).\(format.rawValue)")))
            return
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let readedData = try JSONDecoder().decode(T.self, from: data)
            completion(.success(readedData))
        } catch {
            completion(.failure(.parseError))
        }
    }
}
