//
//  NetworkService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/24/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import Network

class NetworkService {

    private var networkMonitor: NWPathMonitor

    init() {
        networkMonitor = NWPathMonitor()
    }

    func checkConnection(completion: @escaping (NetworkStatus) -> Void) {
        networkMonitor.pathUpdateHandler = { path in
            var status: NetworkStatus = .nan
            if path.status == .satisfied {
                status = .online
            } else {
                status = .offline
            }
            DispatchQueue.main.async {
                completion(status)
            }
        }
        let queue = DispatchQueue(label: "NetworkMonitor")
        networkMonitor.start(queue: queue)
    }

}

enum NetworkStatus {
    case online
    case offline
    case nan
}
