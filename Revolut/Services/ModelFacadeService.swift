//
//  ModelFacadeService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/23/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

protocol ModelFacadeServiceProtocol {
    typealias Response = (ErrorService?) -> Void
    typealias DataResponse<T> = (Result<T, ErrorService>) -> Void

    func getRates(for rates: [ExchangeRate], completion: @escaping Response)
    func saveSelectedExchangeRates(_ rates: [ExchangeRate], completion: @escaping Response)
    func fetchSelectedCurrencies(completion: @escaping DataResponse<[[Currency]]>)
    func readCurrencies(completion: @escaping DataResponse<[Currency]>)
}

class ModelFacadeService: ModelFacadeServiceProtocol {

    private var api: APIServiceProtocol
    private var coreDataService: CoreDataServiceProtocol
    private var fileReader: FileReaderProtocol

    init(_ api: APIServiceProtocol = APIService(),
         coreDataService: CoreDataServiceProtocol = CoreDataService(),
         fileReader: FileReaderProtocol = FileReaderService()) {
        self.api = api
        self.coreDataService = coreDataService
        self.fileReader = fileReader
    }
    
}

// MARK: - API Request
extension ModelFacadeService {

    func getRates(for rates: [ExchangeRate], completion: @escaping Response) {
        let rateString = rates.map { "pairs=\($0.currencyCodeSequence)" }.joined(separator: "&")
        api.getRates(rateString) { [weak self] result in
            switch result {
            case .success(let data):
                self?.manipulateData(data, rates: rates, completion: completion)
            case .failure(let error):
                completion(error)
            }
        }
    }

}

// MARK: - CoreData Requests
extension ModelFacadeService {

    func saveSelectedExchangeRates(_ rates: [ExchangeRate], completion: @escaping Response) {
        coreDataService.saveOrUpdate(rates: rates, completion: completion)
    }

    func fetchSelectedCurrencies(completion: @escaping DataResponse<[[Currency]]>) {
        coreDataService.fetchRates { result in
            switch result {
            case .success(let rates):
                let fetchedCurrencies = rates.map {
                    [$0.leftCurrency, $0.rightCurrency]
                }
                completion(.success(fetchedCurrencies))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    fileprivate func manipulateData(_ data: Dictionary<String, Double>, rates: [ExchangeRate], completion: @escaping Response) {
        let convertableRates = rates.map {($0.currencyCodeSequence, $0)}
        let ratesDictionary = Dictionary(uniqueKeysWithValues: convertableRates)
        var ratesForSave = [ExchangeRate]()
        for (key, value) in data {
            if let rate = ratesDictionary[key] {
                rate.rightCurrency.exchangeAmount = value.roundToDecimal()
                ratesForSave.append(rate)
            }
        }
        if !ratesForSave.isEmpty {
            self.saveSelectedExchangeRates(ratesForSave, completion: completion)
        }
    }

}

// MARK: - File Request
extension ModelFacadeService {

    func readCurrencies(completion: @escaping DataResponse<[Currency]>) {
        fileReader.readCurrencies(from: "currencies", fileFormat: .json) { result in
            switch result {
            case .success(let currencies):
                completion(.success(currencies))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}
