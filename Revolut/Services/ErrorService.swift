//
//  ErrorService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

enum ErrorService: Error, LocalizedError, Equatable {

    case noData,
        fileNotFound(String),
        parseError,
        emptyResponseError,
        pageNotFound,
        serverError,
        incorrectUrl,
        dataSaveError,
        dataDeleteError,
        connectionError,
        defaultError(String)

    var errorDescription: String? {
        switch self {
        case .noData:
            return "Could not fetch data from server or local storage!"
        case .fileNotFound(let fileName):
            return "Could not find file named as \(fileName)"
        case .parseError:
            return "Error occured while parsing data!"
        case .emptyResponseError:
            return "Response is empty!"
        case .pageNotFound:
            return "404: Page not found or incorrect url!"
        case .serverError:
            return "Error occured while connecting to server!"
        case .incorrectUrl:
            return "Request URL is incorrect!"
        case .dataSaveError:
            return "Error while data saving!"
        case .dataDeleteError:
            return "Error while data deleting!"
        case .connectionError:
            return "The Internet connection appears to be offline."
        case .defaultError(let errorMessage):
            return errorMessage

        }
    }

}
