//
//  CoreDataService.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

protocol CoreDataServiceProtocol {
    var context: NSManagedObjectContext { get }
    var persistenceContainer: NSPersistentContainer { get }
    func saveOrUpdate(rates data: [ExchangeRate], completion: @escaping (ErrorService?) -> Void)
    func fetchRates(completion: @escaping (Result<[ExchangeRate], ErrorService>) -> Void)
    func batchDelete<T: NSFetchRequestResult>(in context: NSManagedObjectContext, fetchRequest: NSFetchRequest<T>) throws
}

class CoreDataService: CoreDataServiceProtocol {
    var context: NSManagedObjectContext
    var persistenceContainer: NSPersistentContainer

    init(_ context: NSManagedObjectContext = CoreDataContext.shared.viewContext,
         persistenceContainer: NSPersistentContainer = CoreDataContext.shared.persistentContainer) {
        self.context = context
        self.persistenceContainer = persistenceContainer
    }

    func saveOrUpdate(rates data: [ExchangeRate], completion: @escaping (ErrorService?) -> Void) {

        if data.isEmpty { completion(.none) }

        for rate in data {
            do {
                let fetchRequest: NSFetchRequest<RExchangeRate> = RExchangeRate.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "currencyCodeSequence == %@", rate.currencyCodeSequence)
                let fetchedData = try context.fetch(fetchRequest)
                if let fetchedExchangeRate = fetchedData.first {
                    fetchedExchangeRate.exchangeRate = rate
                } else {
                    let rExchangeRate = RExchangeRate(context: context)
                    rExchangeRate.exchangeRate = rate
                    rExchangeRate.createdAt = Date()
                    rExchangeRate.currencyCodeSequence = rate.currencyCodeSequence
                }
                try context.save()
                completion(.none)
            } catch {
                completion(.dataSaveError)
            }
        }
    }

    func fetchRates(completion: @escaping (Result<[ExchangeRate], ErrorService>) -> Void) {

        let fetchRequest: NSFetchRequest<RExchangeRate> = RExchangeRate.fetchRequest()

        do {
            let fetchedData = try context.fetch(fetchRequest)
            let exchangeRates = fetchedData.compactMap { $0.exchangeRate }
            completion(.success(exchangeRates))
        } catch {
            completion(.failure(.noData))
        }

    }

    func batchDelete<T: NSFetchRequestResult>(in context: NSManagedObjectContext, fetchRequest: NSFetchRequest<T>) throws {
        guard let request = fetchRequest as? NSFetchRequest<NSFetchRequestResult> else {
            throw ErrorService.defaultError("Conversion error occured!")
        }
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        do {
            try context.execute(batchDeleteRequest)
        } catch {
            throw ErrorService.dataDeleteError
        }
    }

}
