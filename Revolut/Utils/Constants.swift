//
//  Constants.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Constants {
    private init() {}

    struct CellIndetifiers {
        static let mainCellID = "MainCellID"
        static let currencyCellID = "CurrencyCellID"
    }
}
