//
//  Storyboards.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

enum Storyboard: String {

    case main = "Main"
    case currency = "Currency"

    func instantiate<T: UIViewController>(_ viewController: T.Type = T.self, bundle: Bundle? = nil) -> T {
        let storyboard = UIStoryboard(name: self.rawValue, bundle: bundle)
        guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: viewController)) as? T else {
            fatalError("Can't instantiate viewController with identifier \(String(describing: viewController))")
        }
        return vc
    }

}
