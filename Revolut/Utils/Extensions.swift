//
//  Extensions.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func customNavigationBar() {
        self.tintColor = UIColor.darkGray
        self.barTintColor = .white
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        self.shadowImage = UIImage()
    }
}

extension UIViewController {

    func errorAlert(with errorMessage: String, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: "Error!", message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Close", style: .cancel) { _ in
            completion?()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }

    func deleteAlert(with errorMessage: String, completion: ((UIAlertAction.Style) -> Void)? = nil) {
        let alertController = UIAlertController(title: "Warning!", message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Delete", style: .destructive) { action in
            completion?(action.style)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            completion?(action.style)
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }

    func trackError(with message: String) {
        // we can print or save to file all logs
        print(message)
    }

}

extension UIColor {

    struct Custom {
        static let mainTextColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
        static let secondaryTextColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.0)
        static let disabledTextColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:1.0)
    }

}

extension Double {
    func roundToDecimal(_ fractionDigits: Int = 4) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
