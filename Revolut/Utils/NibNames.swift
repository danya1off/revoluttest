//
//  NibNames.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

enum NibNames: String {
    case customButtonView = "CustomButtonView"


    var nib: UINib {
        return UINib(nibName: self.rawValue, bundle: nil)
    }

    func instantiate<T: UIView>() -> T {

        guard let view = self.nib.instantiate(withOwner: nib, options: nil).first as? UIView else {
            fatalError("Error while instantiating from Nib \(self.rawValue)")
        }

        guard let requestedView = view as? T else {
            fatalError("Error while casting to type \(T.self)")
        }
        return requestedView
    }

}
