//
//  BaseViewController.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setupView() {

    }

    func errorHandler(_ error: Error) {
        self.trackError(with: error.localizedDescription)
        self.errorAlert(with: error.localizedDescription)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

}
