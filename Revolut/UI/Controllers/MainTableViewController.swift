//
//  MainViewController.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData

class MainTableViewController: BaseTableViewController {

    @IBOutlet private weak var editButton: UIBarButtonItem!

    private var viewModel: MainViewModel?
    private var customButtonView: CustomButtonView?
    private var isTableViewInEditMode = false

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainViewModel()
        self.checkConnection()
        fetchRates()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func setupView() {
        super.setupView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.accessibilityIdentifier = "mainTable"

        customButtonView = NibNames.customButtonView.instantiate()
        customButtonView?.clickAction = { [weak self] in
            self?.turnOffTableViewEditing()
            let currencyViewController = Storyboard.currency.instantiate(CurrenciesTableViewController.self)
            let currencyViewModel = CurrencyViewModel()
            currencyViewController.viewModel = currencyViewModel
            currencyViewController.saveSelectedRates = {
                currencyViewModel.fetchAndSaveRates { [weak self] error in
                    if let error = error {
                        self?.errorHandler(error)
                    }
                }
            }
            let navController = UINavigationController(rootViewController: currencyViewController)
            navController.isModalInPresentation = true
            self?.present(navController, animated: true, completion: nil)
        }
    }

    private func fetchRates() {
        guard let viewModel = viewModel else { return }
        do {
            try viewModel.fetchData(self)
            self.updateView()
        } catch {
            fatalError(error.localizedDescription)
        }
    }

    private func updateView() {
        guard let viewModel = viewModel else { return }
        guard let customButtonView = customButtonView else { return }
        if viewModel.numberOfRows() == 0 {
            self.turnOffTableViewEditing()
            viewModel.invalidateTimer()
            customButtonView.changePosition(.tableViewBG)
            self.tableView.backgroundView = self.customButtonView
        } else {
            customButtonView.changePosition(.headerView)
            self.tableView.backgroundView = nil
            if !tableView.isEditing {
                fireTimer()
            }
        }
    }

    private func turnOffTableViewEditing() {
        tableView.setEditing(false, animated: true)
        isTableViewInEditMode = false
        editButton.title = "Edit"
    }

    private func fireTimer() {
        guard let viewModel = viewModel else { return }
        viewModel.setupTimer { [weak self] error in
            if let error = error {
                viewModel.invalidateTimer()
                self?.errorHandler(error)
            }
        }
    }

    @IBAction private func editAction(_ sender: UIBarButtonItem) {
        guard let viewModel = viewModel else { return }
        if viewModel.numberOfRows() > 0 {
            tableView.setEditing(!tableView.isEditing, animated: true)
            isTableViewInEditMode = tableView.isEditing
            if tableView.isEditing {
                editButton.title = "Cancel"
                viewModel.invalidateTimer()
            } else {
                editButton.title = "Edit"
                fireTimer()
            }
        }
    }

    private func checkConnection() {
        guard let viewModel = viewModel else { return }
        viewModel.checkConnection { [weak self] error in
            if let error = error {
                self?.errorHandler(error)
            }
        }
    }

}

extension MainTableViewController {

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return customButtonView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
}

extension MainTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIndetifiers.mainCellID) as? MainTableViewCell,
        let viewModel = viewModel else { return UITableViewCell() }
        if let cellViewModel = viewModel.getRate(at: indexPath) {
            cell.configure(cellViewModel)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        viewModel?.invalidateTimer()
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, handler) in
            guard let strongSelf = self else { return }
            strongSelf.deleteRate(at: indexPath, handler: handler)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }

    private func deleteRate(at indexPath: IndexPath, handler: @escaping (Bool) -> Void) {
        self.deleteAlert(with: "Are you sure to delete this exchange rate?") { [weak self] action in
            guard let strongSelf = self else { return }
            switch action {
            case .destructive:
                strongSelf.viewModel?.deleteRate(at: indexPath)
                handler(true)
            case .cancel, .default:
                handler(false)
                if !strongSelf.isTableViewInEditMode {
                    strongSelf.fireTimer()
                }
            @unknown default:
                fatalError()
            }
        }
    }

}

extension MainTableViewController: NSFetchedResultsControllerDelegate {

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }


    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        self.updateView()
    }


    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .none)
            }
            tableView.reloadData()
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .none)
            }
        default:
            break
        }
    }

}
