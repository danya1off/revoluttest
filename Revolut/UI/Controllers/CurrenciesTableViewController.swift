//
//  CurrenciesTableViewController.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CurrenciesTableViewController: BaseTableViewController {

    var viewModel: CurrencyViewModel?
    var saveSelectedRates: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel == nil {
            viewModel = CurrencyViewModel()
        }
        getCurrencies()
        fetchRates()
    }

    override func setupView() {
        super.setupView()
        tableView.accessibilityIdentifier = "currencyTable"
        navigationItem.title = "Currencies"
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction(_:)))
    }

    private func getCurrencies() {
        guard let viewModel = viewModel else { return }
        viewModel.getCurrencies { [weak self] error in
            if let error = error {
                self?.errorHandler(error)
            }
            self?.tableView.reloadData()
        }
    }

    private func fetchRates() {
        guard let viewModel = viewModel else { return }
        viewModel.fetchSelectedCurrencies { [weak self] error in
            if let error = error {
                self?.errorAlert(with: error.localizedDescription)
            }
        }
    }

    @objc private func cancelAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true) { [weak self] in
            self?.viewModel?.cancel()
        }
    }

}

extension CurrenciesTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.currenciesCount ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIndetifiers.currencyCellID) as? CurrencyTableViewCell else { return UITableViewCell() }
        self.configure(cell, at: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectRow(at: indexPath)
    }

    private func configure(_ cell: CurrencyTableViewCell, at indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        if let cellViewModel = viewModel.getCurrencyCellViewModel(by: indexPath.row) {
            var isCellDisabled = false
            if viewModel.selectedCurrenciesCount == 1,
                viewModel.getFirstSelectedCurrencyCode == cellViewModel.currencyCode ||
            viewModel.notSelectableCurrencies.contains(cellViewModel.currencyCode) {
                isCellDisabled = true
            }
            cell.configure(cellViewModel, disabled: isCellDisabled)
        }
    }

    private func selectRow(at indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.selectionStyle != UITableViewCell.SelectionStyle.none {
            if viewModel.selectedCurrenciesCount == 0 && viewModel.addCurrency(indexPath.row) {
                let currencyViewController = Storyboard.currency.instantiate(CurrenciesTableViewController.self)
                currencyViewController.viewModel = viewModel
                currencyViewController.saveSelectedRates = saveSelectedRates
                navigationController?.pushViewController(currencyViewController, animated: true)
            } else if viewModel.selectedCurrenciesCount == 1 && viewModel.addCurrency(indexPath.row) {
                self.saveSelectedRates?()
                dismiss(animated: true)
            } else {
                self.errorAlert(with: "Some error occured while selecting the currency!") { [weak self] in
                    self?.dismiss(animated: true)
                }
            }
        }
    }

}
