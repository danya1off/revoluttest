//
//  MainTableViewCell.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet private weak var leftCurrencyCode: UILabel!
    @IBOutlet private weak var leftCurrencyName: UILabel!
    @IBOutlet private weak var rightCurrencyRate: UILabel!
    @IBOutlet private weak var rightCurrencyName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    func configure(_ viewModel: MainCellViewModel) {
        self.leftCurrencyCode.text = viewModel.leftCurrencyCode
        self.leftCurrencyName.text = viewModel.leftCurrencyName
        self.rightCurrencyName.text = viewModel.rightCurrencyName
        self.rightCurrencyRate.text = "\(viewModel.exchangeAmount)"
    }

}
