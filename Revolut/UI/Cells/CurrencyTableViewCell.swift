//
//  CurrencyTableViewCell.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet private weak var flagImageView: UIImageView!
    @IBOutlet private weak var currencyCode: UILabel!
    @IBOutlet private weak var currencyName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configure(_ viewModel: CurrencyCellViewModel, disabled: Bool) {
        self.currencyCode.text = viewModel.currencyCode
        self.currencyName.text = viewModel.currencyName
        self.flagImageView.image = UIImage(named: viewModel.currencyCode)
        if disabled {
            self.currencyCode.textColor = UIColor.Custom.disabledTextColor
            self.currencyName.textColor = UIColor.Custom.disabledTextColor
            self.flagImageView?.alpha = 0.5
            self.selectionStyle = .none
        } else {
            self.currencyCode.textColor = UIColor.Custom.mainTextColor
            self.currencyName.textColor = UIColor.Custom.secondaryTextColor
            self.flagImageView?.alpha = 1
            self.selectionStyle = .default
        }
    }

}
