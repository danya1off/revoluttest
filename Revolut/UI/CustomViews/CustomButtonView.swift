//
//  MainHeaderView.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import UIKit

enum CustomViewPosition {
    case headerView
    case tableViewBG
}

class CustomButtonView: UIView {

    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var infoLabel: UILabel!
    var clickAction: (()-> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        stackView.axis = .horizontal
        infoLabel.isHidden = true
    }

    func changePosition(_ position: CustomViewPosition) {
        switch position {
        case .headerView:
            self.stackView.axis = .horizontal
            infoLabel.isHidden = true
        case .tableViewBG:
            self.stackView.axis = .vertical
            infoLabel.isHidden = false
        }
    }

    @IBAction private func addAction(_ sender: UIButton) {
        clickAction?()
    }

}
