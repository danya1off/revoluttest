//
//  Currency.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

public class Currency: NSObject, Codable, NSCoding {
    var currencyCode: String
    var currencyName: String
    var exchangeAmount = 0.0

    init(currencyCode: String, currencyName: String) {
        self.currencyCode = currencyCode
        self.currencyName = currencyName
    }

    init(currencyCode: String, currencyName: String, exchangeAmount: Double) {
        self.currencyCode = currencyCode
        self.currencyName = currencyName
        self.exchangeAmount = exchangeAmount
    }

    enum CodingKeys: String, CodingKey {
        case currencyCode = "code"
        case currencyName = "name"
    }

    required public convenience init?(coder aDecoder: NSCoder) {
        guard
            let currencyCode = aDecoder.decodeObject(forKey: Keys.currencyCodeKey.rawValue) as? String,
            let currencyName = aDecoder.decodeObject(forKey: Keys.currencyNameKey.rawValue) as? String
            else { return nil }
        let exchangeAmount = aDecoder.decodeDouble(forKey: Keys.exchangeAmountKey.rawValue)
        self.init(currencyCode: currencyCode, currencyName: currencyName, exchangeAmount: exchangeAmount)
    }

    public func encode(with coder: NSCoder) {
        coder.encode(currencyCode, forKey: Keys.currencyCodeKey.rawValue)
        coder.encode(currencyName, forKey: Keys.currencyNameKey.rawValue)
        coder.encode(exchangeAmount, forKey: Keys.exchangeAmountKey.rawValue)
    }

    enum Keys: String {
        case currencyCodeKey = "currencyCode"
        case currencyNameKey = "currencyName"
        case exchangeAmountKey = "exchangeAmount"
    }
}

extension Currency {
    static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.currencyCode == rhs.currencyCode
    }
}
