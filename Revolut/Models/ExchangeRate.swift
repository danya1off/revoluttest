//
//  ExchangeRate.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/22/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation

public class ExchangeRate: NSObject, Codable, NSCoding {

    var leftCurrency: Currency
    var rightCurrency: Currency
    var currencyCodeSequence: String

    init(leftCurrency: Currency, rightCurrency: Currency) {
        self.leftCurrency = leftCurrency
        self.rightCurrency = rightCurrency
        self.currencyCodeSequence = "\(leftCurrency.currencyCode)\(rightCurrency.currencyCode)"
    }

    init(leftCurrency: Currency, rightCurrency: Currency, codeSequence: String) {
        self.leftCurrency = leftCurrency
        self.rightCurrency = rightCurrency
        self.currencyCodeSequence = codeSequence
    }

    convenience required public init?(coder aDecoder: NSCoder) {
        guard
            let leftCurrency = aDecoder.decodeObject(forKey: Keys.leftCurrencyKey.rawValue) as? Currency,
            let rightCurrency = aDecoder.decodeObject(forKey: Keys.rightCurrencyKey.rawValue) as? Currency,
            let currencyCodeSequence = aDecoder.decodeObject(forKey: Keys.currencyCodeSequence.rawValue) as? String
            else { return nil }
        self.init(leftCurrency: leftCurrency, rightCurrency: rightCurrency, codeSequence: currencyCodeSequence)
    }

    public func encode(with coder: NSCoder) {
        coder.encode(leftCurrency, forKey: Keys.leftCurrencyKey.rawValue)
        coder.encode(rightCurrency, forKey: Keys.rightCurrencyKey.rawValue)
        coder.encode(currencyCodeSequence, forKey: Keys.currencyCodeSequence.rawValue)
    }

    enum Keys: String {
        case leftCurrencyKey = "leftCurrency"
        case rightCurrencyKey = "rightCurrency"
        case currencyCodeSequence = "currencyCodeSequence"
    }
}
