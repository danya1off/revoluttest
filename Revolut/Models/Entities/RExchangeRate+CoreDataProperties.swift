//
//  RExchangeRate+CoreDataProperties.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/25/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension RExchangeRate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RExchangeRate> {
        return NSFetchRequest<RExchangeRate>(entityName: "RExchangeRate")
    }

    @NSManaged public var createdAt: Date
    @NSManaged public var currencyCodeSequence: String
    @NSManaged public var exchangeRate: ExchangeRate
    @NSManaged public var leftCurrency: RCurrency
    @NSManaged public var rightCurrency: RCurrency

}
