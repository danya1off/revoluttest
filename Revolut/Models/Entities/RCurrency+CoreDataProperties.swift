//
//  RCurrency+CoreDataProperties.swift
//  Revolut
//
//  Created by Jeyhun Danyalov on 10/25/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension RCurrency {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RCurrency> {
        return NSFetchRequest<RCurrency>(entityName: "RCurrency")
    }

    @NSManaged public var currency: Currency
    @NSManaged public var leftRate: RExchangeRate
    @NSManaged public var rightRate: RExchangeRate

}
