//
//  Currency.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/27/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrenciesTableViewControllerTest: XCTestCase {

    private var sut: CurrenciesTableViewController!
    private var bridge: Bridge!

    override func setUp() {
        super.setUp()
        sut = Storyboard.main.instantiate(CurrenciesTableViewController.self)
        sut.loadViewIfNeeded()
        bridge = Bridge(CoreDataService())
    }

    override func tearDown() {
        super.tearDown()
    }

}
