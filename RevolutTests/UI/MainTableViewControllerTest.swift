//
//  MainTableViewControllerTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class MainTableViewControllerTest: XCTestCase {

    private var sut: MainTableViewController!
    private var bridge: Bridge!

    override func setUp() {
        super.setUp()
        sut = Storyboard.main.instantiate(MainTableViewController.self)
        sut.loadViewIfNeeded()
        bridge = Bridge(CoreDataService())
    }

    override func tearDown() {
        super.tearDown()
    }

}
