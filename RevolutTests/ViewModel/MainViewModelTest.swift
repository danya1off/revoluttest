//
//  MainViewModel.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class MainViewModelTest: XCTestCase {

    private var sut: MainViewModel!
    private var vc: MainTableViewController!
    private var coreDataService: CoreDataServiceProtocol!
    private var bridge: Bridge!

    override func setUp() {
        super.setUp()
        vc = Storyboard.main.instantiate(MainTableViewController.self)
        vc.loadViewIfNeeded()
        coreDataService = CoreDataService()
        bridge = Bridge(coreDataService)
        if TestConstants.testWithMock {
            let bundle = Bundle(for: type(of: self))
            let fileReader = FileReaderService(bundle)
            let mockApi = MockAPIService()
            let facade = ModelFacadeService(mockApi, fileReader: fileReader)
            sut = MainViewModel(facade)
        } else {
            sut = MainViewModel()
        }

    }

    override func tearDown() {
        sut = nil
        vc = nil
        coreDataService = nil
        bridge = nil
        super.tearDown()
    }

    func testWhichCheckNSFetchedResultControllerWithoutData() {
        bridge.flushAllData()

        do {
            try sut.fetchData(vc)
        } catch {
            XCTFail()
        }
        XCTAssertEqual(sut.numberOfRows() == 0, true)
    }

    func testWhichCheckNSFetchedResultControllerWithData() {
        let expectation = self.expectation(description: "check fetched result controller with data")

        bridge.flushAllData()
        let testData = TestConstants.prepareMockData()
        bridge.saveUpdateData(testData, expectation: expectation)

        do {
            try sut.fetchData(vc)
        } catch {
            XCTFail()
        }
        XCTAssertEqual(sut.numberOfRows() > 0, true)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichCheckIsExchangeRateFetchableFromNSFetchedResultController() {
        self.testWhichCheckNSFetchedResultControllerWithData()
        let exchangeRate = sut.getRate(at: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(exchangeRate)
        XCTAssertEqual(exchangeRate!.leftCurrencyCode, "USD")
    }

}
