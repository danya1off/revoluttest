//
//  CurrencyViewModelTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/27/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrencyViewModelTest: XCTestCase {

    private var sut: CurrencyViewModel!
    private var vc: MainTableViewController!
    private var coreDataService: CoreDataServiceProtocol!
    private var bridge: Bridge!

    override func setUp() {
        super.setUp()
        vc = Storyboard.main.instantiate(MainTableViewController.self)
        vc.loadViewIfNeeded()
        coreDataService = CoreDataService()
        bridge = Bridge(coreDataService)
        if TestConstants.testWithMock {
            let bundle = Bundle(for: type(of: self))
            let fileReader = FileReaderService(bundle)
            let mockApi = MockAPIService()
            let facade = ModelFacadeService(mockApi, fileReader: fileReader)
            sut = CurrencyViewModel(facade)
        } else {
            sut = CurrencyViewModel()
        }

    }

    override func tearDown() {
        sut = nil
        vc = nil
        coreDataService = nil
        bridge = nil
        super.tearDown()
    }

    func testWhichGetCurrenciesFromJsonFile() {
        let expectation = self.expectation(description: "read currencies from json file")

        bridge.flushAllData()
        getCurrenciesTest(expectation)
        XCTAssertEqual(sut.currenciesCount > 0, true)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }

    }

    func testWhichAddCurrenciesToSelectedCurrenciesArray() {
        self.testWhichGetCurrenciesFromJsonFile()
        addCurrenciesTest()
    }

    func testWhichFetchExchangeRatesForSelectedCurrenciesFromRestApiAndSaveInCoreData() {
        let readExpectation = self.expectation(description: "read currencies from json file")
        let fetchExpectation = self.expectation(description: "get rates for selected currencies from rest api")

        bridge.flushAllData()
        getCurrenciesTest(readExpectation)
        addCurrenciesTest()
        fetchAndSaveTest(fetchExpectation)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichFetchAllSelectedExchangeRatesFromCoreData() {
        let readExpectation = self.expectation(description: "read currencies from json file")
        let fetchFromApiExpectation = self.expectation(description: "get rates for selected currencies from rest api")
        let fetchExpectation = self.expectation(description: "get rates for selected currencies from core data")

        bridge.flushAllData()
        getCurrenciesTest(readExpectation)
        addCurrenciesTest()
        fetchAndSaveTest(fetchFromApiExpectation)

        sut.fetchSelectedCurrencies { (error) in
            XCTAssertNil(error)
            fetchExpectation.fulfill()
        }

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichDeleteSelectedCurrenciesIfUserPressCancelButton() {
        self.testWhichAddCurrenciesToSelectedCurrenciesArray()
        sut.cancel()
        XCTAssertEqual(sut.selectedCurrenciesCount == 0, true)
    }

    func testWhichSelectCurrencyCellViewModel() {
        let expectation = self.expectation(description: "get currency cell view model")
        bridge.flushAllData()
        self.getCurrenciesTest(expectation)
        let cellViewModel = sut.getCurrencyCellViewModel(by: 0)
        XCTAssertNotNil(cellViewModel)
        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    private func getCurrenciesTest(_ expectation: XCTestExpectation) {
        sut.getCurrencies { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
    }

    private func fetchAndSaveTest(_ expectation: XCTestExpectation) {
        sut.fetchAndSaveRates { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
    }

    private func addCurrenciesTest() {
        let addLeftCurrency = sut.addCurrency(0)
        XCTAssertEqual(addLeftCurrency, true)
        let addRightCurrency = sut.addCurrency(1)
        XCTAssertEqual(addRightCurrency, true)
    }

    

}
