//
//  ExtensionsTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class ExtensionsTest: XCTestCase {

    private var sut: UITableViewController!

    override func setUp() {
        super.setUp()
        sut = Storyboard.main.instantiate(MainTableViewController.self)
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testCutomErrorUIAlertControllerWithoutCompletion() {
        sut.errorAlert(with: "Error alert")
    }

    func testCutomDeleteUIAlertControllerWithoutCompletion() {
        sut.deleteAlert(with: "Delete alert")
    }

    func testCutomTrackError() {
        sut.trackError(with: "Track error")
    }

    func testCustomRoundDecimal() {
        let double = 1.111111111
        XCTAssertEqual(double.roundToDecimal(4), 1.1111)
    }

    func testErrorService() {
        XCTAssertNotNil(ErrorService.noData.localizedDescription)
        XCTAssertNotNil(ErrorService.fileNotFound("File not found").localizedDescription)
        XCTAssertNotNil(ErrorService.parseError.localizedDescription)
        XCTAssertNotNil(ErrorService.emptyResponseError.localizedDescription)
        XCTAssertNotNil(ErrorService.pageNotFound.localizedDescription)
        XCTAssertNotNil(ErrorService.serverError.localizedDescription)
        XCTAssertNotNil(ErrorService.incorrectUrl.localizedDescription)
        XCTAssertNotNil(ErrorService.dataSaveError.localizedDescription)
        XCTAssertNotNil(ErrorService.dataDeleteError.localizedDescription)
        XCTAssertNotNil(ErrorService.connectionError.localizedDescription)
        XCTAssertNotNil(ErrorService.defaultError("Default error").localizedDescription)
    }

}
