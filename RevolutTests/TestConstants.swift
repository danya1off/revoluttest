//
//  TestConstants.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
@testable import Revolut

struct TestConstants {
    private init() {}

    static let timeIntervale: TimeInterval = 2
    static let testWithMock = false

    static func prepareMockData() -> ExchangeRate {
        let leftCurrency = Currency(currencyCode: "USD", currencyName: "US Dollar", exchangeAmount: 1.1)
        let rightCurrency = Currency(currencyCode: "EUR", currencyName: "Euro", exchangeAmount: 1.1)
        let rate = ExchangeRate(leftCurrency: leftCurrency, rightCurrency: rightCurrency)
        return rate
    }

    static let jsonString = "{\"USDEUR\":0.7895}"
    
}
