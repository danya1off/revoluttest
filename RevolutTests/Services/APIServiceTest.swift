//
//  APIServiceTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class APIServiceTest: XCTestCase {

    private var sut: APIServiceProtocol!

    override func setUp() {
        super.setUp()
        sut = TestConstants.testWithMock ? MockAPIService() : APIService()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testWhichFetchDataFromRestAPIServer() {
        let expectation = self.expectation(description: "fetch data from rest api")

        let mockRateString = "pairs=GBPUSD"
        sut.getRates(mockRateString) { result in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                XCTAssertEqual(data.count > 0, true)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

}

class MockAPIService: APIServiceProtocol {
    func getRates(_ rates: String, completion: @escaping ResponseResult<Dictionary<String, Double>>) {
        
        guard let data = TestConstants.jsonString.data(using: .utf8) else {
            completion(.failure(.parseError))
            return
        }
        do {
            let mockData = try JSONDecoder().decode([String: Double].self, from: data)
            completion(.success(mockData))
        }catch {
            completion(.failure(ErrorService.parseError))
        }

    }

}
