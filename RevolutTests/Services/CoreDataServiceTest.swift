//
//  CoreDataServiceTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
import CoreData
@testable import Revolut

class CoreDataServiceTest: XCTestCase {

    private var sut: CoreDataServiceProtocol!
    private var bridge: Bridge!
    private var context: NSManagedObjectContext!
    private var container: NSPersistentContainer!

    override func setUp() {
        super.setUp()
        context = CoreDataContext.shared.viewContext
        container = CoreDataContext.shared.persistentContainer
        sut = CoreDataService(context, persistenceContainer: container)
        bridge = Bridge(sut)
    }

    override func tearDown() {
        bridge.flushAllData()
        sut = nil
        super.tearDown()
    }

    func testWhichCheckSaveContextFunctionality() {
        bridge.flushAllData()
        let exchangeRate = TestConstants.prepareMockData()
        let rExchangeRate = RExchangeRate(context: context)
        rExchangeRate.exchangeRate = exchangeRate
        CoreDataContext.shared.saveContext()
    }

    func testWhichDeleteAllDataFromCoreData() {
        bridge.flushAllData()
    }

    func testWhichCheckSaveADataInCoreData() {
        let saveExpectation = self.expectation(description: "save exchange rate in core data")
        let fetchExpectation = self.expectation(description: "fetch exchange rate from core data")

        bridge.flushAllData()

        let exchangeRate = TestConstants.prepareMockData()
        bridge.saveUpdateData(exchangeRate, expectation: saveExpectation)
        self.fetchData(fetchExpectation)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichCheckFetchFromCoreDataFunctionality() {
        self.testWhichCheckSaveADataInCoreData()
    }

    func testWhichCheckUpdateDataInCoreData() {
        let saveExpectation = self.expectation(description: "save exchange rate in core data")
        let updateExpectation = self.expectation(description: "update exchange rate in core data")

        bridge.flushAllData()

        let exchangeRate = TestConstants.prepareMockData()
        bridge.saveUpdateData(exchangeRate, expectation: saveExpectation)

        let updateExchangeRate = TestConstants.prepareMockData()
        updateExchangeRate.rightCurrency.exchangeAmount = 2.0
        bridge.saveUpdateData(updateExchangeRate, expectation: updateExpectation)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    private func fetchData(_ expectation: XCTestExpectation) {
        sut.fetchRates { result in
            switch result {
            case .success(let rates):
                if rates.isEmpty {
                    XCTFail("Data is empty")
                }
                XCTAssertNotEqual(rates.count, 0)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
    }

}
