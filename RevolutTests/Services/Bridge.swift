//
//  File.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData
import XCTest
@testable import Revolut

class Bridge {

    private var sut: CoreDataServiceProtocol

    init(_ sut: CoreDataServiceProtocol) {
        self.sut = sut
    }

    func flushAllData() {
        let context = CoreDataContext.shared.viewContext
        let fetchRequest: NSFetchRequest<RExchangeRate> = RExchangeRate.fetchRequest()
        do {
            try sut.batchDelete(in: context, fetchRequest: fetchRequest)
        } catch {
            XCTFail()
        }
    }

    func saveUpdateData(_ data: ExchangeRate, expectation: XCTestExpectation) {
        sut.saveOrUpdate(rates: [data]) { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
    }

}
