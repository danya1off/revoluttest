//
//  RevolutTests.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class RouterTest: XCTestCase {

    func testIfGetRatesRequestIsSuccess() {
        let correctRatesString = "pairs=USDGBP"
        let router = Router.getRates(correctRatesString)
        do {
            let request = try router.request()
            XCTAssertNotNil(request.allHTTPHeaderFields)
            let value = request.value(forHTTPHeaderField: "Content-Type")
            XCTAssertEqual(value, "application/json")
        } catch {
            XCTFail()
        }
    }

    func testIfWrongURLThrowAnError() {
        let wrongRatesString = "+"
        let router = Router.getRates(wrongRatesString)
        do {
            _ = try router.request()
        } catch {
            XCTAssertThrowsError(error)
        }
    }

}
