//
//  FileReaderTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class FileReaderTest: XCTestCase {

    private var sut: FileReaderProtocol!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: type(of: self))
        sut = FileReaderService(bundle)
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testWhichReadCurrenciesFromJsonFile() {
        let expectation = self.expectation(description: "read currencies from json file")

        self.readFile("currencies", expectation: expectation)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichShouldFailsIfFileIsNotFound() {
        let expectation = self.expectation(description: "read currencies from json file")

        self.readFile("mockFile", expectation: expectation)

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    private func readFile(_ file: String, expectation: XCTestExpectation) {
        sut.readCurrencies(from: file, fileFormat: .json) { result in
            switch result {
            case .success(let data):
                XCTAssertEqual(data.count > 0, true)
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertEqual(error, ErrorService.fileNotFound("\(file).json"))
            }
            expectation.fulfill()
        }
    }



}
