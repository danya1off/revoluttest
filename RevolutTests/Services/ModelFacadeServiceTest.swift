//
//  ModelFacadeServiceTest.swift
//  RevolutTests
//
//  Created by Jeyhun Danyalov on 10/26/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Revolut

class ModelFacadeServiceTest: XCTestCase {

    private var sut: ModelFacadeServiceProtocol!
    private var coreDataService: CoreDataServiceProtocol!
    private var bridge: Bridge!

    override func setUp() {
        super.setUp()
        coreDataService = CoreDataService()
        bridge = Bridge(coreDataService)
        if TestConstants.testWithMock {
            let mockApi = MockAPIService()
            let bundle = Bundle(for: type(of: self))
            let fileReader = FileReaderService(bundle)
            sut = ModelFacadeService(mockApi, fileReader: fileReader)
        } else {
            sut = ModelFacadeService()
        }
    }

    override func tearDown() {
        bridge.flushAllData()
        sut = nil
        super.tearDown()
    }

    func testWhichFetchExchangeRatesFromRestAPI() {
        let expectation = self.expectation(description: "get exchange rates from rest api")

        let exchangeRate = TestConstants.prepareMockData()
        sut.getRates(for: [exchangeRate]) { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }

    }

    func testWhichShouldFailIfWePathWrongData() {
        // should be tested with real api calls
        if !TestConstants.testWithMock {
            let expectation = self.expectation(description: "get exchange rates from rest api")

            let exchangeRate = TestConstants.prepareMockData()
            exchangeRate.currencyCodeSequence = ""
            sut.getRates(for: [exchangeRate]) { error in
                XCTAssertNotNil(error)
                expectation.fulfill()
            }

            self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
                if let error = error {
                    XCTFail(error.localizedDescription)
                }
            }
        }
    }

    func testWhichFetchSelectedCurrenciesFromCoreData() {
        let fetchExpectation = self.expectation(description: "fetch selected currencies from CoreData")
        let saveExpectation = self.expectation(description: "save test data to CoreData")

        bridge.flushAllData()

        let testData = TestConstants.prepareMockData()
        bridge.saveUpdateData(testData, expectation: saveExpectation)

        sut.fetchSelectedCurrencies { result in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                XCTAssertEqual(data.count > 0, true)
                fetchExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testWhichReadCurrenciesFromJsonFile() {
        let expectation = self.expectation(description: "read currencies from json file")

        sut.readCurrencies { result in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                XCTAssertEqual(data.count > 0, true)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        self.waitForExpectations(timeout: TestConstants.timeIntervale) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

}
