//
//  RevolutUITests.swift
//  RevolutUITests
//
//  Created by Jeyhun Danyalov on 10/21/19.
//  Copyright © 2019 Jeyhun Danyalov. All rights reserved.
//

import XCTest

class MainTableViewControllerUITest: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false

        app = XCUIApplication()
        app.launch()

    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }

    func testWhichAddNewExchangeRateToMainTableAndDeleteIt() {
        let mainTable = app.tables["mainTable"]
        self.addNewExchangeRateToMainTable(mainTable)
        mainTable.cells.element(boundBy: 0).swipeLeft()
        mainTable.buttons["Delete"].tap()
        app.alerts["Warning!"].scrollViews.otherElements.buttons["Delete"].tap()
        XCTAssertEqual(mainTable.tableRows.count == 0, true)

    }

    func testWhichAddNewExchangeRateToMainTableAndDeleteItByEditButton() {
        let mainTable = app.tables["mainTable"]
        self.addNewExchangeRateToMainTable(mainTable)
        app.buttons["Edit"].tap()
        mainTable.cells.element(boundBy: 0).children(matching: .button).element.tap()
        mainTable.buttons["Delete"].tap()
        app.alerts["Warning!"].buttons["Delete"].tap()
        XCTAssertEqual(mainTable.tableRows.count == 0, true)

    }

    func testWhichAddNewExchangeRateToMainTableAndPressCancelWhenDeleteWarningAppears() {
        let mainTable = app.tables["mainTable"]
        self.addNewExchangeRateToMainTable(mainTable)
        app.buttons["Edit"].tap()
        mainTable.cells.element(boundBy: 0).children(matching: .button).element.tap()
        mainTable.buttons["Delete"].tap()
        app.alerts["Warning!"].buttons["Cancel"].tap()
        XCTAssertEqual(mainTable.cells.count == 1, true)
    }

    func testWhichCheckEditCancelButtonToSwitchTableViewEditable() {
        let mainTable = app.tables["mainTable"]
        self.addNewExchangeRateToMainTable(mainTable)
        app.buttons["Edit"].tap()
        mainTable.cells.element(boundBy: 0).children(matching: .button).element.tap()
        mainTable.buttons["Delete"].tap()
        app.alerts["Warning!"].buttons["Cancel"].tap()
        app.buttons["Cancel"].tap()
    }

    func testWhichCheckCancelActionWhenUserDecidedToNotSelectCurrency() {
        let mainTable = app.tables["mainTable"]
        self.clearMainTableView(mainTable)
        let currencyTable = app.tables["currencyTable"]
        XCTAssertEqual(mainTable.cells.count == 0, true)
        mainTable.children(matching: .button).element.tap()
        XCTAssertEqual(currencyTable.cells.count > 0, true)
        app.buttons["Cancel"].tap()
        XCTAssertEqual(mainTable.cells.count == 0, true)
    }

    private func addNewExchangeRateToMainTable(_ mainTable: XCUIElement) {
        self.clearMainTableView(mainTable)
        let currencyTable = app.tables["currencyTable"]
        XCTAssertEqual(mainTable.cells.count == 0, true)
        mainTable.children(matching: .button).element.tap()
        XCTAssertEqual(currencyTable.cells.count > 0, true)
        currencyTable.cells.element(boundBy: 0).tap()
        currencyTable.cells.element(boundBy: 1).tap()
        mainTable.tap()
    }

    private func clearMainTableView(_ mainTable: XCUIElement) {
        if mainTable.cells.count > 0 {
            for i in 0..<mainTable.cells.count {
                mainTable.cells.element(boundBy: i).swipeLeft()
                mainTable.buttons["Delete"].tap()
                app.alerts["Warning!"].scrollViews.otherElements.buttons["Delete"].tap()
            }
        }
    }
}
